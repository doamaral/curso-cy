describe("Feature: Fill Ticket ordering form", ()=>{
    beforeEach(() => {
        cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html")
    })

    it("Email field Behaviour to Valid Email", ()=>{
        cy.get("#email")
            .as("email")
            .type('asd')

        cy.get("#email.invalid") 
            .should("exist")
        
        cy.get("@email")
            .clear()
            .type("asdf@gmail.com")
    
        cy.get("#email.invalid")    
            .should("not.exist")
    })

    it("Verify Full Name, when it's filled, at Purchase Agreement", (first = "Lucas", last = "Amaral")=>{
        const first_name = first
        const last_name = last

        cy.get("#first-name").type(first_name)
        cy.get("#last-name").type(last_name)
        cy.get("div.agreement p").should("contain", `${first_name} ${last_name}`)
    })

    it("Verify Full Name, when it's filled and cleared, at Purchase Agreement", (first = "Lucas", last = "Amaral")=>{
        const first_name = first
        const last_name = last

        cy.get("#first-name").type(first_name)
        cy.get("#last-name").type(last_name)
        cy.get("div.agreement p").should("contain", `${first_name} ${last_name}`)
        
        cy.get("#first-name").clear()
        cy.get("#last-name").clear()
        cy.get("div.agreement p").should("not.contain", `${first_name} ${last_name}`)
    })

    it("Email field Behaviour to invalid Email", ()=>{
        cy.get("#email").type('asd')
        cy.get("#email.invalid").should("exist")
    })

    it("Has 'ticketbox' header", ()=>{
        cy.get("header h1").should("contain", "TICKETBOX")
    })
    
    it("Typing text", ()=>{
        const first_name = 'Lucas'
        const last_name = 'Amaral'
    
        cy.get("#first-name").type(first_name)
        cy.get("#last-name").type(last_name)
        cy.get("#email").type('lucasamaral.cm@gmail.com')
        cy.get("#requests").type('Especial Space')
        cy.get("#signature").type(`${first_name} ${last_name}`)
    })

    it("Select 'vip' Radio button",()=>{
        cy.get("#vip").check()
    })
    it("Select 'Social media' Checkbox",()=>{
        cy.get("#social-media").check()
        //cy.get("#social-media").click()
        //funciona tb com click()
    })
    it("Unchecking checkbox",()=>{
        cy.get("#friend").check()
        cy.get("#publication").check()
        cy.get("#friend").uncheck()

        // cy.get("#friend").click()
        // cy.get("#publication").click()
        // cy.get("#friend").click()
    })
})

describe("Testing Clone 1", ()=>{
    beforeEach(() => cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"))
    
    it("do nothing", ()=>{
        //nada!
    })
    it("do nothing 2", ()=>{
        //nada!
    })
    it("do nothing 3", ()=>{
        //nada!
    })
    it("do nothing 4", ()=>{
        //nada!
    })
})

describe("Testing Clone 2", ()=>{
    beforeEach(() => cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"))
    
    it.skip("do nothing", ()=>{
        //nada!
    })
    it("do nothing 2", ()=>{
        //nada!
    })
    it("do nothing 3", ()=>{
        //nada!
    })
    it("do nothing 4", ()=>{
        //nada!
    })
})
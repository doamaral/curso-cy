module.exports = {
    e2e: {
      setupNodeEvents(on, config) {
        // implement node event listeners here
      },
      baseUrl: "https://www.saucedemo.com",
      chromeWebSecurity: false,
      viewportWidth: 1369,
      viewportHeight: 769,
      video: false
    },
  };
  
